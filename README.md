# Serverless Project

개발 n년차 운영 배포에 신경 안쓰고 비즈니스 로직에만 신경쓰고 싶습니다.  
클라우드 서비스 공급자가 여럿 있지만 여러 옵션들을 제공해주는 AWS Lamda로 serverless를 셋팅하겠습니다.   
리전은 도쿄를 사용합니다. 왜냐하면 최신 기능들은 한국에 바로 서비스되지 않고, AWS 한국 리전에 서비스 에러가 발생한 이력이 있습니다.

### serverless cli 설치
```bash
npm install -g serverless 
```

### 프로젝트 생성
```bash
nest new study-serverless-api
```

### AWS 사용자 셋팅
* IAM 계정 생성하여 aws_access_key_id와, aws_secret_access_key를 발급 받습니다.
```bash
cd study-serverless-api

# AWS에서 발급받은 인증 정보를 셋팅 합니다.
sls config credentials --provider aws --key aws_access_key_id --secret aws_secret_access_key
```


### NestJS App Lambda 셋팅
* src/ 하위 디렉토리에 NestJS를 실행하는 Handler를 생성 합니다.
```typescript
// src/lambda.ts
// lambda.ts
import { Context, Handler } from 'aws-lambda';
import { Server } from 'http';
import { createServer, proxy } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';

import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';

const express = require('express');

// NOTE: If you get ERR_CONTENT_DECODING_FAILED in your browser, this is likely
// due to a compressed response (e.g. gzip) which has not been handled correctly
// by aws-serverless-express and/or API Gateway. Add the necessary MIME types to
// binaryMimeTypes below
const binaryMimeTypes: string[] = [];

let cachedServer: Server;

async function bootstrapServer(): Promise<Server> {
  if (!cachedServer) {
    const expressApp = express();
    const nestApp = await NestFactory.create(
      AppModule,
      new ExpressAdapter(expressApp),
    );
    nestApp.use(eventContext());
    await nestApp.init();
    cachedServer = createServer(expressApp, undefined, binaryMimeTypes);
  }
  return cachedServer;
}

export const handler: Handler = async (event: any, context: Context) => {
  cachedServer = await bootstrapServer();
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};

```
이제 하나의 람다 엔트리 포인트가 생성 되었습니다.

* serverless.yaml 파일을 생성하고 아래 코드를 붙여넣습니다.
```yaml
# ~/serverless.yaml
service: study-serverless-lambda

plugins:
  - 'serverless-plugin-typescript'
  - serverless-plugin-optimize
  - serverless-offline

provider:
  name: aws
  runtime: nodejs14.x
  region: ap-northeast-1

functions:
  main: # The name of the lambda function
    # The module 'handler' is exported in the file 'src/lambda'
    handler: src/lambda.handler

    events:
      - http:
          method: any
          path: /{any+}

  scheduler-test:
    handler: src/lambda.schedulerTest
    events:
      #- schedule: rate(2 hours) # offline에서 rate 함수로 스케줄링은 됩니다.
      - schedule: cron(*/1 * * * ? *)  # offline에서 실행 시 에러가 뜨는데 AWS에 베포하면 실행됩니다. 아래 GET 엔드포인트로 테스트합니다. (minutes hour day-of-month month day-of-week year)
      - http:
          method: get
          path: /schedule/schedulerTest

custom:
  serverless-offline:
    httpPort: 8000

```
위 serverless.yaml로 인하여 nestJS의 모든 API가 람다로 라우팅 됩니다.

* 다음은 람다 실행을 위해 몇 가지 패키지를 설치 합니다.
  * aws-serverless-express: NodeJS에 존재하는 API를 활용하여 AWS Lambda와 API Gateway를 사용할 수 있게 합니다.
  * aws-lambda: 서버 없이 바로 코드를 실행 할 수 있게 합니다.
  * serverless-plugin-typescript: 다른 컴파일러나 플러그인을 설치할 필요 없이 바로 Typescript를 지원하는 서버리스 플러그인 힙니다.
  * serverless-plugin-optimize: 코드를 변환하고 축소하는 플러그인 입니다.
  * serverless-offline plugin: 오프라인에서 앱 테스트를 할 수 있는 플러그인 입니다.

```bash
npm install --save aws-serverless-express
npm install --save aws-lambda
npm install --save-dev serverless-plugin-typescript
npm install --save-dev serverless-plugin-optimize
npm install --save-dev serverless-offline
```

* tsconfig.json에서 incremental 를 삭제합니다.
```json
{
  "compilerOptions": {
    "module": "commonjs",
    "declaration": true,
    "removeComments": true,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "allowSyntheticDefaultImports": true,
    "target": "es2017",
    "sourceMap": true,
    "outDir": "./dist",
    "baseUrl": "./",
    "skipLibCheck": true,
    "strictNullChecks": false,
    "noImplicitAny": false,
    "strictBindCallApply": false,
    "forceConsistentCasingInFileNames": false,
    "noFallthroughCasesInSwitch": false
  }
}
```

### lambda 실행
* 오프라인 실행
```bash
sls offline start
```

* 배포
```bash
sls deploy

# 개발환경
sls deploy --stage=development

# 운영환경
sls deploy --stage=product

# 함수별 배포(AWS CloudFormation 없이 개별 함수를 배포 합니다. CloudFormation와 동기화되지 않으므로 프로덕션 배포에 사용 X) 
sls deploy function -f main --stage=development 
```

* 삭제
```
sls remove
```

* 로그 가져오기
함수 호출로 인한 모든 로그는 AWS CloudWatch에 자동으로 저장됩니다. 다음 명령어를 통해 해당 로그를 검색 합니다.
```bash
sls log -f main

sls log -f main --tail
```


### 스케줄링
```yaml
# serverless.yaml


service: study-serverless-lambda

plugins:
  - 'serverless-plugin-typescript'
  - serverless-plugin-optimize
  - serverless-offline
  - serverless-local-schedule

provider:
  name: aws
  runtime: nodejs14.x
  region: ap-northeast-1

functions:
  main: # The name of the lambda function
    # The module 'handler' is exported in the file 'src/lambda'
    handler: src/lambda.handler

    events:
      - http:
          method: any
          path: /{any+}

  scheduler-test:
    handler: src/lambda.schedulerTest
    events:
      #- schedule: rate(2 hours) # offline에서 rate 함수로 스케줄링은 된다.
      - schedule: cron(*/1 * * * ? *)  # offline에서 실행 시 에러가 뜨는데 AWS에 베포하면 실행된다. 아래 GET 엔드포인트로 테스트한다. (minutes hour day-of-month month day-of-week year)
      - http:
          method: get
          path: /schedule/schedulerTest

custom:
  serverless-offline:
    httpPort: 8000

```
